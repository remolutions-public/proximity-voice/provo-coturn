# What is this?
This ist the webrtc turn/stun server for the provo electron app


# Official Provo website
https://provo.remolutions.com/

[![alt text](https://provo.remolutions.com/assets/logos/Provo_64p.png "Provo")](https://provo.remolutions.com/)



# Port Range for turnserver
49152-65535:49152-65535/udp


- https://github.com/coturn/coturn
- https://hub.docker.com/r/instrumentisto/coturn



## Provo Discord Server
- https://discord.gg/NJTFTgNvzG



## Donation/Support
If you like my work and want to support further development or just to spend me a coffee please

[![alt text](https://i.imgur.com/Y0XkUcd.png "Paypal $")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S3WQNNSVY8VAL)

[![alt text](https://i.imgur.com/xezX26q.png "Paypal €")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VQRPA46YADD9J)



# How to create turn server tls secret
deploy the api project to auto generate the tls cert via cert-manager...
