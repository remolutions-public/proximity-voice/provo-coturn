#!/bin/sh

if [ ! -z "$EXTERNAL_IPV6" ]; then
  echo -n "$EXTERNAL_IPV6"
  exit 0
fi

if [ -z "$REAL_EXTERNAL_IPV6" ]; then
  export REAL_EXTERNAL_IPV6="$(curl -6 https://icanhazip.com 2>/dev/null)"
fi

exec echo -n "$REAL_EXTERNAL_IPV6"

