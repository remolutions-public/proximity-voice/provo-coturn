#!/bin/sh

EXTERNAL_DNS_IP=1.1.1.1

MAIN_NIC_NAME=$(ip route get $EXTERNAL_DNS_IP | sed -nr 's/.*dev ([^\ ]+).*/\1/p')
export NIC_IPV4_ADDRESS=$(ip addr show dev $MAIN_NIC_NAME | grep -m1 inet | egrep -o '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+' | head -n 1)

exec echo -n "$NIC_IPV4_ADDRESS"
