#!/bin/sh


if [ -z "$COTURN_PORT_RANGE_MIN" ]; then
  COTURN_PORT_RANGE_MIN=49152
fi
if [ -z "$COTURN_PORT_RANGE_MAX" ]; then
  COTURN_PORT_RANGE_MAX=65535
fi


rm -f /etc/turnserver.conf
cp /etc/turnserver_DEV.conf /etc/turnserver.conf
CONFIG_FILE=/etc/turnserver.conf
export EXTERNAL_IPV4=$(sh /usr/local/bin/detect-external-ipv4.sh)
export EXTERNAL_IPV6=$(sh /usr/local/bin/detect-external-ipv6.sh)
export REDIS_IP=$(kubectl get svc $REDIS_HOST -o jsonpath='{ .spec.clusterIP }')
sed -i 's/#EXTERNAL_IPV4#/'"$EXTERNAL_IPV4"'/g' $CONFIG_FILE
if [ ! -z "$EXTERNAL_IPV6" ]; then
  sed -i 's/#HAS_IPV6#//g' $CONFIG_FILE
  sed -i 's/#EXTERNAL_IPV6#/'"$EXTERNAL_IPV6"'/g' $CONFIG_FILE
fi
sed -i 's/#APP_PORT#/'"$APP_PORT"'/g' $CONFIG_FILE
sed -i 's/#APP_TLS_PORT#/'"$APP_TLS_PORT"'/g' $CONFIG_FILE
sed -i 's/#COTURN_PORT_RANGE_MIN#/'"$COTURN_PORT_RANGE_MIN"'/g' $CONFIG_FILE
sed -i 's/#COTURN_PORT_RANGE_MAX#/'"$COTURN_PORT_RANGE_MAX"'/g' $CONFIG_FILE
sed -i 's/#TURN_HOSTNAME#/'"$TURN_HOSTNAME"'/g' $CONFIG_FILE
sed -i 's/#STUN_HOSTNAME#/'"$STUN_HOSTNAME"'/g' $CONFIG_FILE
sed -i 's/#ENV_HOST_NAME#/'"$ENV_HOST_NAME"'/g' $CONFIG_FILE
sed -i 's/#REALM#/'"$DOMAIN"'/g' $CONFIG_FILE
sed -i 's/#CLI_PASSWORD#/'"$CLI_PASSWORD"'/g' $CONFIG_FILE


# if no cert is mounted, create a fake one
if [ ! -f "/usr/local/etc/pkey.pem" ]; then
  echo "creating turn server cert..."
  mkdir -p /usr/local/etc
  openssl req -x509 -newkey rsa:4096 -keyout "/usr/local/etc/pkey.pem" -out "/usr/local/etc/cert.pem" -days 3650 -nodes -subj "/C=None/ST=None/L=None/O=$CERT_IDENTIFICATION/OU=$CERT_IDENTIFICATION/CN=$CERT_IDENTIFICATION/emailAddress=$EMAIL"
  chmod 666 /usr/local/etc/pkey.pem
  chmod 666 /usr/local/etc/cert.pem
fi


/usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf
