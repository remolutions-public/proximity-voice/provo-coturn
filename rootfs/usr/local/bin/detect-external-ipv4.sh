#!/bin/sh

REAL_EXTERNAL_IPV4=$ENV_HOST_IP

if [ -z "$REAL_EXTERNAL_IPV4" ]; then
  export REAL_EXTERNAL_IPV4="$(curl -4 https://icanhazip.com 2>/dev/null)"
fi

exec echo -n "$REAL_EXTERNAL_IPV4"