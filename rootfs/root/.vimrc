set hi=100

filetype on
" Syntax hilighting
syntax on

:colorscheme ron

set pastetoggle=<F10>

" Statuszeile
set ruler
" dazugeh?¶e Klammer bei ) ] oder } zeige
set showmatch
" Statuszeile 2 Zeilen hoch
set ch=2
set ls=2
" Nicht 100%ig vi-kompatibel
set nocompatible
" Beim Suchen die gefundenen W?¶r highlite
set hlsearch
" 5 Zeilen um den cursor herumscrollen
set so=1
" Visual Bell = nicht piepsen, sondern screen aufblitzen lassen
set vb
" when completing tags in Insert mode show more info
set sft
" Show partial commandkey on status-line
set sc
" Pfad f?¼ei mit W?¶
set dict=/usr/dict/words
set nu

set bs=indent,eol,start
set is

set backupdir=/tmp
set directory=/tmp

" Toggle fold state between closed and opened.
"
" If there is no fold at current line, just moves forward.
" If it is present, reverse it's state.
fun! ToggleFold()
  if foldlevel('.') == 0
    normal! l
  else
    if foldclosed('.') < 0
    . foldclose
    else
    . foldopen
    endif
  endif
  " Clear status line
  echo
endfun

set foldlevel=0
set foldcolumn=2
"set foldclose=all
set foldmethod=indent
set foldminlines=5
set foldnestmax=1
set shiftwidth=2
set foldcolumn=0
set nofen

" Mit strg+f wird dateiname unter des Cursors in neuem Fensterle ge?¶et
nnoremap f :split <cfile><CR>
"nnoremap f :split <cfile><CR>

noremap <Tab> <C-w>w
noremap <C-a> <C-W>
noremap <C-x> <C-W>c
noremap <C-n> <C-W>n
noremap <space> :call ToggleFold()<CR>

map! <C-?> <C-H>
map <C-?> <C-H>

noremap ] :set nofen<CR>:set foldcolumn=0<CR>
noremap [ :set fen<CR>:set foldcolumn=2<CR>
map <C-B> :set nonu<CR>

noremap ^[[4~ <End>
inoremap ^[[4~ <End>
noremap ^[[1~ <Home>
inoremap ^[[1~ <Home>

noremap ^[[1;5H <C-Home>
noremap ^[[1;5F <C-End>
inoremap ^[[1;5H <C-Home>
inoremap ^[[1;5F <C-End>
nnoremap <Tab> <C-w>w
nnoremap ^[[1;5B <C-w>-
nnoremap ^[[1;5A <C-w>+
inoremap ^[[1;5C <C-Right>
inoremap ^[[C <C-Right>
nnoremap ^[[C <C-Right>
inoremap ^[[D <C-Left>
nnoremap ^[[D <C-Left>
inoremap ^[[1;5D <C-Left>
noremap ^[[1;5C <C-Right>
noremap ^[[1;5D <C-Left>

:vnoremap > >gv
:vnoremap < <gv
" legt strg+p auf shift+tab && strg+n auf tab bzw. tab auf tab (wenn vor dem cursor kein zeichen steht)
function! InsertTabWrapper(direction)
    let col = col('.') - 1
    if !col || getline('.')[col - 1] !~ '\k'
        return "\<tab>"
    elseif "backward" == a:direction
        return "\<c-p>"
    else
        return "\<c-n>"
    endif
endfunction
inoremap <tab> <c-r>=InsertTabWrapper ("forward")<cr>
inoremap <s-tab> <c-r>=InsertTabWrapper ("backward")<cr>
" Funktionsliste f?¼ zur autovervollst?¤igung (keine Gew?¤ auf korrektheit der schreibweise)
set dictionary-=~/funclist.txt dictionary+=~/funclist.txt
set complete-=k complete+=k
" Indent mit Tab einschalten und jeweiligen Indent halten
set et
set sta
set ai

set fileencodings=ucs-bom,utf-8
set fileencoding=
set nobomb
set enc=utf-8
if has("statusline")
set statusline=%<%f\ %h%m%r%=%{\"[\".(&fenc==\"\"?&enc:&fenc).((exists(\"+bomb\")\ &&\ &bomb)?\",B\":\"\").\"]\ \"}%k\ %-14.(%l,%c%V%)\ %P
endif
" FARB KRAMS

"highlight Comment ctermfg=black ctermbg=lightgray
"highlight Constant ctermfg=white
"highlight Search ctermfg=black ctermbg=magenta


" remapping fuer den kleinen insert mode damit man nicht immer aus der
" grundpos gehen muss =)
map ^[[A <up>
imap ^[[B <down>
imap ^[[D <left>
imap ^[[C <right>
map ^[[A <up>
map ^[[B <down>
map ^[[D <left>
map ^[[C <right>
vmap ^[[A <up>
vmap ^[[B <down>
vmap ^[[D <left>
vmap ^[[C <right>
cmap ^[[A <up>
cmap ^[[B <down>
cmap ^[[D <left>
cmap ^[[C <right>
imap <C-h> <left>
imap <C-j> <down>
imap <C-k> <up>
imap <C-l> <right>
imap <C-w> <home>
imap <C-e> <end>
imap <C-?> <backspace>
