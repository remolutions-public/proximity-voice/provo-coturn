#!/bin/sh
# create new turn_secret

if [ -z "$1" ]; then
  exit 0
fi

turnadmin -k -u $1 -r $2 -p $3

# turnadmin -k -u user -r <DOMAIN> -p <PASSWORD>
