#!/bin/sh

if [ -z "$1" ]; then
  echo "no user provided"
  exit 0
fi

if [ -z "$2" ]; then
  echo "no realm provided"
  exit 0
fi

if [ -z "$3" ]; then
  echo "no password provided"
  exit 0
fi

turnadmin -a -u $1 -r $2 -p $3
