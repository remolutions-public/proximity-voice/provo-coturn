const { curly } = require('node-libcurl');
const execa = require('execa');
const db = require('better-sqlite3')('/var/lib/coturn/turndb');
process.on('exit', () => db.close());
process.on('SIGHUP', () => process.exit(128 + 1));
process.on('SIGINT', () => process.exit(128 + 2));
process.on('SIGTERM', () => process.exit(128 + 15));
let turnPwExp = process.env.TURN_PW_EXPIRATION_MS;
const realm = process.env.DOMAIN;
const user = process.env.TURN_USERNAME;

const podname = process.env.ENV_POD_NAME;
const turnHostname = process.env.TURN_HOSTNAME;
const stunHostname = process.env.STUN_HOSTNAME;
const nodeName = process.env.ENV_HOST_NAME;

// const turnUserTable = "turnusers_lt";
// // OBSOLETE
// // const remove = db.prepare(
// //   `DELETE FROM ${turnUserTable} WHERE realm = $realm AND name = $name`
// // );
// const insert = db.prepare(
//   `INSERT INTO ${turnUserTable} (realm, name, hmackey) VALUES ($realm, $name, $hmackey)`
// );
// const update = db.prepare(
//   `UPDATE ${turnUserTable} SET hmackey = $hmackey WHERE realm = $realm AND name = $name`
// );
// const select = db.prepare(
//   `SELECT * FROM ${turnUserTable} WHERE realm = $realm AND name = $name`
// );

/*
 * main exec block
 */
(async () => {
  const adminUser = 'admin';
  const adminPw = await execa('sh', ['/node/src/pwgen.sh']);
  await execa('sh', [
    '/node/src/upsert-admin.sh',
    adminUser,
    realm,
    adminPw.stdout,
  ]);

  let genpw;
  const debounceRefs = {};
  const upsertUser = async () => {
    if (debounceRefs.upsertUser) {
      clearTimeout(debounceRefs.upsertUser);
    }

    genpw = await execa('sh', ['/node/src/pwgen.sh']);
    await execa('sh', ['/node/src/upsert-user.sh', user, realm, genpw.stdout]);

    const encPw = await execa('sh', ['/node/src/encrypt-pw.sh', genpw.stdout]);
    const pruneTurnSecret = db.prepare(`DELETE FROM turn_secret`);
    pruneTurnSecret.run();
    const insertTurnSecret = db.prepare(
      `INSERT INTO turn_secret (realm, value) VALUES ($realm, $value)`
    );
    insertTurnSecret.run({
      realm,
      value: encPw.stdout,
    });

    debounceRefs.upsertUser = setTimeout(() => {
      upsertUser();
    }, turnPwExp || 300000);
  };

  const updateRedisEntry = async () => {
    if (debounceRefs.updateRedisEntry) {
      clearTimeout(debounceRefs.updateRedisEntry);
    }

    if (
      !(genpw == undefined) &&
      !(genpw.stdout == undefined) &&
      // !(pwHmacKey == undefined) &&
      !(user == undefined) &&
      !(realm == undefined)
    ) {
      const jsonObj = {
        ip: process.env.ENV_HOST_IP,
        ipv6: process.env.EXTERNAL_IPV6,
        port: process.env.APP_PORT,
        tlsport: process.env.APP_TLS_PORT,
        turnHostname,
        stunHostname,
        realm, // domain
        nodeName,
        user,
        // hmacpw: pwHmacKey,
        pw: genpw.stdout,
        adminUser,
        adminPw: adminPw.stdout,
      };

      curly
        .post(`${process.env.API_HOST}/api/v1/coturn?podname=${podname}`, {
          httpHeader: [
            'Content-Type: application/json',
            `secret: ${process.env.API_GUARD_SECRET}`,
          ],
          postFields: JSON.stringify(jsonObj),
        })
        .then((result) => {
          if (result?.statusCode == undefined || result?.statusCode >= 400) {
            console.info(
              new Date().toISOString() +
                ' - Could not send data - ' +
                JSON.stringify(result)
            );
          }
        })
        .catch((err) => {
          console.error(new Date().toISOString() + ' - ' + err.message || err);
        });
    }

    debounceRefs.updateRedisEntry = setTimeout(() => {
      updateRedisEntry();
    }, Math.random() * 500 + 10000);
  };

  upsertUser();
  updateRedisEntry();
})();
