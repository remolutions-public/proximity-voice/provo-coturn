#!/bin/sh

if [ -z "$1" ]; then
  exit 0
fi

turnadmin -P -p $1
