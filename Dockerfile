# https://hub.docker.com/r/instrumentisto/coturn/dockerfile
FROM alpine:3.18

ARG COTURN_VERSION=4.6.2
ARG KUBECTL_VERSION=1.25.9


# Build and install Coturn.
RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
        ca-certificates \
        curl \
        nodejs \
        npm \
        supervisor \
        make \
        build-base \
        gcc \
        wget \
        git \
        screen \
        bash \
        vim \
        sqlite \
        openssl \
  && update-ca-certificates \
    \
  # Install Coturn dependencies.
  && apk add --no-cache \
        libevent \
        libcrypto1.1 libssl1.1 \
        libpq sqlite-libs \
    \
  # Install tools for building.
  && apk add --no-cache --virtual .tool-deps \
        coreutils autoconf g++ libtool make \
    \
  # Install Coturn build dependencies.
  && apk add --no-cache --virtual .build-deps \
        linux-headers \
        libevent-dev \
        openssl-dev \
        postgresql-dev sqlite-dev \
    \
  # Download and prepare Coturn sources.
  && curl -fL -o /tmp/coturn.tar.gz \
        https://github.com/coturn/coturn/archive/${COTURN_VERSION}.tar.gz \
  && tar -xzf /tmp/coturn.tar.gz -C /tmp/ \
  && cd /tmp/coturn-* \
    \
  # Build Coturn from sources.
  && ./configure --prefix=/usr \
        --turndbdir=/var/lib/coturn \
        --disable-rpath \
        --sysconfdir=/etc/coturn \
        # No documentation included to keep image size smaller.
        --mandir=/tmp/coturn/man \
        --docsdir=/tmp/coturn/docs \
        --examplesdir=/tmp/coturn/examples \
  && make \
    \
  # Install and configure Coturn.
  && make install \
  # Preserve license file.
  && mkdir -p /usr/share/licenses/coturn/ \
  && cp /tmp/coturn/docs/LICENSE /usr/share/licenses/coturn/ \
  # Remove default config file.
  && rm -f /etc/coturn/turnserver.conf.default \
    \
  # Cleanup unnecessary stuff.
  && apk del .tool-deps .build-deps \
  && rm -rf /var/cache/apk/* \
          /tmp/*


# install kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
    && mv kubectl /usr/bin/kubectl \
    && chmod +x /usr/bin/kubectl


COPY rootfs /
# explicitely copy hidden config files
COPY rootfs/root/.bashrc /root/.bashrc
COPY rootfs/root/.vimrc /root/.vimrc
COPY rootfs/root/.screenrc /root/.screenrc


RUN chmod +x /usr/local/bin/* \
    && ln -s /usr/local/bin/detect-external-ipv4.sh \
            /usr/local/bin/detect-external-ip


# install node server
COPY src /node/src
COPY package*.json /node
RUN npm i --prefix /node


# supervisord will start the turnserver
ENTRYPOINT ["docker-entrypoint.sh"]
